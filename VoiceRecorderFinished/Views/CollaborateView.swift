//
//  CollaborateView.swift
//  VoiceRecorderFinished
//
//  Created by Sijan on 23/4/20.
//  Copyright © 2020 Andreas Schultz. All rights reserved.
//

import SwiftUI
import AVFoundation

struct CollaborateView: View {
    
    @ObservedObject var audioRecorder = AudioRecorder()
    @ObservedObject var audioPlayer = AudioPlayer()
    var audioUrl: URL!
    
    var body: some View {
        
        VStack{
            
            Text("Collaborate")
                .font(.largeTitle)
            
            if audioRecorder.recording == false {
                Button(action: {
                    self.audioRecorder.isCollaboring = true
                    print(self.audioRecorder.startRecording())
                    if self.audioPlayer.isPlaying == false {
                        self.audioPlayer.startPlayback(audio: self.audioUrl)
                    }
                }) {
                    Image(systemName: "circle.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipped()
                        .foregroundColor(.red)
                        .padding(.bottom, 40)
                }
            } else {
                
                Button(action: {
                    self.audioRecorder.stopRecording()
                    self.audioPlayer.stopPlayback()
                    
                    let playbackSession = AVAudioSession.sharedInstance()
                    var isHeadPhoneConnected = false
                    
                    for output in playbackSession.currentRoute.outputs where output.portType == AVAudioSessionPortHeadphones {
                        isHeadPhoneConnected = true
                        print("headphone plugged in")
                        break
                    }
                    
                    if isHeadPhoneConnected{
                        let abc = self.audioRecorder.recordings.last!.fileURL
                        self.audioRecorder.mergeFilesWithUrl(recordedUrl: abc as NSURL, audioUrl: self.audioUrl! as NSURL)
                        self.audioRecorder.deleteRecording(urlsToDelete: [abc])
                    }
                    
                    
                }) {
                    Image(systemName: "stop.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipped()
                        .foregroundColor(.red)
                        .padding(.bottom, 40)
                }
            }
            
        }
    }
}

struct CollaborateView_Previews: PreviewProvider {
    static var previews: some View {
        CollaborateView()
    }
}
