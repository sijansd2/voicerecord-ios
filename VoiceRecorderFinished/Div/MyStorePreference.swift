//
//  MyStorePreference.swift
//  VoiceRecorderFinished
//
//  Created by Sijan on 23/4/20.
//  Copyright © 2020 Andreas Schultz. All rights reserved.
//

import Foundation


class MyStorePreference: NSObject {
    
    let preferences = UserDefaults.standard

    let currentLevelKey = "currentLevel"
    
    func saveData(nowPlayingUrl: URL) {
        preferences.set(nowPlayingUrl, forKey: currentLevelKey)
        preferences.synchronize()
    }
    
    
    func getSavedData() -> URL{
        var currentLevel: URL!
        if preferences.object(forKey: currentLevelKey) == nil {
            //  Doesn't exist
        } else {
            currentLevel = preferences.url(forKey: currentLevelKey)
        }
        
        return currentLevel
    }
}
